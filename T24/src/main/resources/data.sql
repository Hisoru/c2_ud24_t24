DROP table IF EXISTS empleado;

create table empleado(
	id int auto_increment primary key,
	nombre varchar(250),
	apellido varchar(250),
	puesto varchar(250),
	salario integer
);

insert into empleado (nombre, apellido,puesto,salario)values('Annalise','Mullen','Applications Programmer',1829);
insert into empleado (nombre, apellido,puesto,salario)values('Aayan','Cairns','Computer Language Coder',1910);
insert into empleado (nombre, apellido,puesto,salario)values('Louisa','Wells','Computer Programmer',1400);
insert into empleado (nombre, apellido,puesto,salario)values('Mitchell','Haines','Junior Software Developer',1837);
insert into empleado (nombre, apellido,puesto,salario)values('Kenya','Miranda','Mainframe Programmer',1637);