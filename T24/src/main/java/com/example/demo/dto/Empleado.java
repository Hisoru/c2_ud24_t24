package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.*;

@Entity
@Table(name="empleado")
public class Empleado {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column (name = "nombre")
	private String nombre;
	
	@Column (name = "apellido")
	private String apellido;
	
	@Column (name = "puesto")
	private String puesto;
	
	@Column (name = "salario")
	private int salario;
	
	public Empleado() {
		
	}
	
	/**
	 * @param id
	 * @param nombre
	 * @param apellido
	 * @param puesto
	 */
	
	public Empleado(Long id, String nombre, String apellido, String puesto) {
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.puesto = puesto;
		this.salario = 0;
	}
	
	/**
	 * @param id
	 * @param nombre
	 * @param apellido
	 * @param puesto
	 * @param salario
	 */
	public Empleado(Long id, String nombre, String apellido, String puesto, int salario) {
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.puesto = puesto;
		this.salario = salario;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public int getSalario() {
		return salario;
	}

	public void setSalario(int salario) {
		this.salario = salario;
	}

	@Override
	public String toString() {
		return "empleado [id=" + id + ", nombre=" + nombre + ", apellido=" + apellido + ", puesto=" + puesto
				+ ", salario=" + salario + "]";
	}
	
	public static int asignarSalario(String puesto) {
		int salario = 0;
		
		if(puesto.contentEquals("Applications Programmer")) {
			salario = 1829;
		}else if(puesto.contentEquals("Computer Language Coder")) {
			salario = 1910;
		}else if(puesto.contentEquals("Computer Programmer")) {
			salario = 1400;
		}else if(puesto.contentEquals("Junior Software Developer")) {
			salario = 1837;
		}else if(puesto.contentEquals("Mainframe Programmer")) {
			salario = 1637;
		}
		
		return salario;
		
	}
	
	
}
