package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.Empleado;

public interface EmpleadoDao extends JpaRepository<Empleado, Long> {
	
	public List<Empleado> findByPuesto(String puesto);
	
}

