package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Empleado;
import com.example.demo.service.EmpleadoImpl;

@RestController
@RequestMapping("/api")
public class EmpleadoController {
	
	@Autowired
	EmpleadoImpl empleadoImpl;
	
	@GetMapping("/Empleado")
	public List<Empleado> listarEmpleado(){
		return empleadoImpl.listarEmpleado();
	}
	
	@GetMapping("/Empleado/puesto/{empleado}")
	public List<Empleado> buscarPuesto(@PathVariable(name = "puesto") String puesto){
		return empleadoImpl.buscarPuesto(puesto);
	}
	
	@PostMapping("/Empleado")
	public Empleado salvarCliente(@RequestBody Empleado empleado) {
		
		return empleadoImpl.guardarEmpleado(empleado);
	}
	
	@GetMapping("/Empleado/{id}")
	public Empleado empleadoXId(@PathVariable(name = "id") Long id) {
		
		Empleado empleado_xid = new Empleado();
		
		empleado_xid = empleadoImpl.empleadoXID(id);
		
		System.out.println("Empleado por ID: " + empleado_xid);
		
		return empleado_xid;
	}
	
	@GetMapping("/Empleado/{puesto}")
	public Empleado empleadoXPuesto(@PathVariable(name = "puesto") String puesto) {
		
		Empleado empleado_xpuesto = new Empleado();
		
		System.out.println("Empleado por ID: " + empleado_xpuesto);
		
		return empleado_xpuesto;
	}
	
	@PutMapping("/Empleado/{id}")
	public Empleado actualizarEmpleado(@PathVariable(name="id")Long id,@RequestBody Empleado empleado) {
		
		Empleado empleadoSeleccionado = new Empleado();
		Empleado empleadoActualizado = new Empleado();
		
		empleadoSeleccionado = empleadoImpl.empleadoXID(id);
		
		empleadoSeleccionado.setNombre(empleado.getNombre());
		empleadoSeleccionado.setApellido(empleado.getApellido());
		empleadoSeleccionado.setPuesto(empleado.getPuesto());
		empleadoSeleccionado.setSalario(Empleado.asignarSalario(empleado.getPuesto()));
		
		empleadoActualizado = empleadoImpl.actualizarEmpleado(empleadoSeleccionado);
		
		System.out.println("El empleado actualizado es: " + empleadoActualizado);
		
		return empleadoActualizado;
		
	}
	
	@DeleteMapping("/Empleado/{id}")
	public void eliminarEmpleado(@PathVariable(name="id")Long id) {
		empleadoImpl.eliminarEmpleado(id);
	}
	
	
}
