package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.EmpleadoDao;
import com.example.demo.dto.Empleado;

@Service
public class EmpleadoImpl {
	
	@Autowired
	EmpleadoDao empleadoDao;

	public List<Empleado> listarEmpleado() {
		return empleadoDao.findAll();
	}
	
	public List<Empleado> buscarPuesto(String puesto) {
		return empleadoDao.findByPuesto(puesto);
	}
	
	public Empleado guardarEmpleado(Empleado empleado) {
		empleado.setSalario(Empleado.asignarSalario(empleado.getPuesto()));
		return empleadoDao.save(empleado);
	}

	public Empleado empleadoXID(Long id) {
		return empleadoDao.findById(id).get();
	}

	public Empleado actualizarEmpleado(Empleado empleado) {
		
		return empleadoDao.save(empleado);
	}

	public void eliminarEmpleado(Long id) {
		
		empleadoDao.deleteById(id);
		
	}
}
