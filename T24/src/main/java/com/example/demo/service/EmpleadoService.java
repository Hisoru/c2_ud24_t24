package com.example.demo.service;

import java.util.List;
import com.example.demo.dto.Empleado;

public interface EmpleadoService {
	
	public List<Empleado> listarEmpleado();
	
	public Empleado guardarEmpleado(Empleado empleado);
	
	public Empleado empleadoXId(Long id);
	
	public Empleado actualizarEmpleado(Empleado empleado);
	
	public void eliminarEmpleado(Long id);
}
